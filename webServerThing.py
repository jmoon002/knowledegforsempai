import threading
import time
import socketserver

class ThreadedTCPRequestHandler(socketserver.BaseRequestHandler):
    def handle(self):
        self.data = self.request.recv(1024).strip()
        print("%s wrote: " % self.client_address[0])
        print(self.data)
        self.request.send(self.data.upper())
        h = str(self.data)
        file2write = open("webOutput", 'w')
        file2write.write(h)
        file2write.close()
class ThreadedTCPServer(socketserver.ThreadingMixIn, socketserver.TCPServer):
    pass

HOST = ""
PORT_A = 80

server_A = ThreadedTCPServer((HOST, PORT_A), ThreadedTCPRequestHandler)

server_A_thread = threading.Thread(target=server_A.serve_forever)

server_A_thread.setDaemon(True)

server_A_thread.start()


while 1:
    time.sleep(1)